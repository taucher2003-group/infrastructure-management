FROM node:18.0.0-buster AS build
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install --frozen-lockfile

FROM node:18.0.0-buster
COPY --from=build node_modules/ node_modules/
ADD index.js index.js
CMD ["node", "index.js"]
