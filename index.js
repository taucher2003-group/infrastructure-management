const { spawnSync } = require("child_process");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
    { name: 'dry-run', type: Boolean },
    { name: 'file', alias: 'f', type: String, multiple: true }
];

const programOptions = commandLineArgs(optionDefinitions);

const results = {
    successfulCommands: 0,
    failedCommands: 0,
    successfulRuns: 0,
    failedRuns: 0,
    exitCode: 0
};

if(programOptions.file && programOptions.file.length) {
    run(programOptions.file, "./");
    process.exit(0);
}

const path = require('path');
const fs = require('fs');
const directoryPath = path.join(__dirname, 'runs');
const files = readDirectoryRecursive(directoryPath, `${__dirname}/runs`);
run(files);
process.exitCode = results.exitCode;

// --- end auto running code
// --- below are only functions which get called from above

function readDirectoryRecursive(directory, currentPath) {
    const dirents = fs.readdirSync(directory, { withFileTypes: true });

    const files = dirents.filter(dirent => dirent.isFile()).map(dirent => `${dirent.name}`);
    const directories = dirents.filter(dirent => dirent.isDirectory());
    directories.forEach(dir => {
        const recursedFiles = readDirectoryRecursive(path.join(currentPath, dir.name), `${currentPath}/${dir.name}`).map(filename => `${dir.name}/${filename}`);
        files.push(...recursedFiles);
    });
    return files;
}

function run(files, directoryPrefix = "./runs/") {
    const startTime = Math.floor(Date.now() / 1000);
    for(const file of files) {
        const result = processFile(`${directoryPrefix}${file}`);
        if(result !== 0) {
            results.exitCode = result;
            break;
        }
    }
    const endTime = Math.floor(Date.now() / 1000);
    const fn = results.exitCode === 0 ? success : error;
    console.info();
    if (results.exitCode === 0) {
        info(`Successful in ${endTime - startTime} seconds`);
    } else {
        info(`Failed (${results.exitCode}) in ${endTime - startTime} seconds`);
    }
    fn("=== [Summary] ===");
    fn(`Successful commands: ${results.successfulCommands}`);
    fn(`Successful runs: ${results.successfulRuns}`);
    fn(`Failed commands: ${results.failedCommands}`);
    fn(`Failed runs: ${results.failedRuns}`);

    if(programOptions['dry-run']) {
        info("This was a dry run, success of the commands may differ from a real run!");
    }
    process.exit(results.exitCode);
}

function processFile(file) {
    if(!/.json$/.test(`${file}`)) {
        return 0;
    }
    info(`Processing ${file}`);
    const json = require(`${file}`);
    return executeConfiguration(json);
}

function executeConfiguration(json) {
    if (!Array.isArray(json)) {
        const result = runConfiguration(json);
        // noinspection JSUnresolvedVariable
        if(result !== 0 && !json.allow_failure) {
            return result;
        }
        return 0;
    }
    for(const configuration of json) {
        const result = runConfiguration(configuration);
        // noinspection JSUnresolvedVariable
        if(result !== 0 && !configuration.allow_failure) {
            return result;
        }
    }
    return 0;
}

function runConfiguration({host, user, port, identity, executions}) {
    if(host === 'local' && executions.some(e => e.type === 'file')) {
        error("File transfer not supported for local host");
        return -1;
    }

    if(host === 'local') {
        for(const execution of executions) {
            expandVariablesInObject(execution);
            const commandSplitted = execution.command.split(" ");
            const command = commandSplitted.shift();
            const cwd = execution.directory;
            const result = runCommand(command, commandSplitted, cwd ? {cwd} : {});
            if(!execution.allow_failure && result !== 0) {
                results.failedRuns += 1;
                return result;
            }
        }
        results.successfulRuns += 1;
        return 0;
    }

    if(!executions || !executions.length) {
        error("Host with no file transfers and commands found");
        return -1;
    }

    for (const execution of executions) {
        expandVariablesInObject(execution);
        if(execution.type === 'file' || execution.type === 'folder') {
            const options = ["-P", `${port}`, "-i", `${identity}`, `${execution.local}`, `${user}@${host}:${execution.remote}`];
            if(execution.type === 'folder') {
                options.unshift('-r');
            }
            // noinspection JSUnresolvedVariable
            const result = runCommand(
                "scp",
                options,
                { hideOutput: execution.hide_output, hideErrors: execution.hide_errors}
            );
            // noinspection JSUnresolvedVariable
            if(!execution.allow_failure && result !== 0) {
                results.failedRuns += 1;
                return result;
            }
        } else if(execution.type === 'command') {
            const options = [`${user}@${host}`, "-p", `${port}`, "-i", `${identity}`];
            // noinspection JSUnresolvedVariable
            const result = runCommand(
                "ssh",
                [...options, execution.command],
                { hideOutput: execution.hide_output, hideErrors: execution.hide_errors }
            );
            // noinspection JSUnresolvedVariable
            if(!execution.allow_failure && result !== 0) {
                results.failedRuns += 1;
                return result;
            }
        }
    }
    results.successfulRuns += 1;
    return 0;
}

function runCommand(command, options, {hideOutput, hideErrors, cwd} = {}) {
    success(`$ ${command} ${options.join(' ')}`);
    if(programOptions['dry-run']) {
        results.successfulCommands += 1;
        return 0; // we don't want the commands to be executed in a dry run
    }
    const startTime = Math.floor(Date.now() / 1000)
    // noinspection JSCheckFunctionSignatures
    const process = spawnSync(command, options, cwd ? {cwd} : {});
    const endTime = Math.floor(Date.now() / 1000);
    if(!hideOutput && `${process.stdout}`) {
        console.info(`${process.stdout}`)
    }
    if(!hideErrors && `${process.stderr}`) {
        console.error(`${process.stderr}`);
    }
    if (process.status === 0) {
        success(`==> ${command} ${options.join(' ')} succeeded in ${endTime - startTime} seconds`);
        results.successfulCommands += 1;
        return 0;
    } else if(process.signal) {
        error(`==> ${command} ${options.join(' ')} interrupted (${process.signal}) after ${endTime - startTime} seconds`);
        results.failedCommands += 1;
        return -1;
    } else {
        error(`==> ${command} ${options.join(' ')} failed (${process.status}) in ${endTime - startTime} seconds`);
        results.failedCommands += 1;
        return process.status;
    }
}

function expandVariablesInObject(object) {
    for(const entry of Object.entries(object)) {
        object[entry[0]] = expandVariables(entry[1]);
    }
}

function expandVariables(originalString) {
    let resultString = `${originalString}`;
    let startSearchIndex = 0;
    while (resultString.indexOf("$", startSearchIndex) > -1) {
        const startIndex = resultString.indexOf("$", startSearchIndex);
        let endIndex = startIndex;
        for(let i = 0; i <= resultString.length - startIndex; i++) {
            const char = resultString.charAt(startIndex + i);
            if(/[^$A-Za-z_]/.test(char) || char === "") {
                endIndex += i;
                break;
            }
        }
        const varToFind = resultString.substring(startIndex, endIndex);
        if(varToFind === "") {
            break;
        }
        const varName = varToFind.replace(/^\$/, "");
        if(process.env[varName]) {
            resultString = resultString.replace(`${varToFind}`, process.env[varName]);
        } else {
            startSearchIndex = endIndex;
        }
    }
    return resultString;
}

function info(message) {
    console.info("\033[0;34m" + message + "\033[0m");
}

function success(message) {
    console.info("\033[0;32m" + message + "\033[0m");
}

function error(message) {
    console.info("\033[0;31m" + message + "\033[0m");
}
